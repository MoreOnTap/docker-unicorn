@working_dir = '/application'
@config_dir = '/etc/unicorn'

listen 3000

worker_processes 4

working_directory @working_dir

pid "#{@config_dir}/unicorn.pid"

stderr_path "#{@config_dir}/error.log"
stdout_path "#{@config_dir}/access.log"

timeout 30
